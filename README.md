# Ansible role - w32time
This role setups w32time service, sets timezone and NTP synchronization.

[[_TOC_]]

## Description
This role supports Windows systems (tested on `munikypo/windows-server-2019` image). You can find the supported timezones in 
[docs](https://docs.microsoft.com/en-us/previous-versions/windows/embedded/ms912391(v=winembedded.11)?redirectedfrom=MSDN),
or in `tzutil.exe /l` output.
For NTP synchronization, you can use either a NTP pool (recommended) or an individual server.

## Requirements
* This role requires root access, so you either need to specify `become` directive as a global or while invoking the role.

    ```yml
    become: yes
    ```

## Parameters

Optional.

- `w32time_timezone` - timezone to be set. Default: `UTC`
- `w32time_ntp_server` - NTP server/pool to use for synchronization. Default: `1.debian.pool.ntp.org`

## Example

The simplest example of NTP timezone setting.

```yaml
roles:
  - role: w32time
    w32time_timezone: Central Europe Standard Time
    w32time_ntp_server: pool.ntp.org
    become: yes
```

## License

Copyright (c) KYPO Team. All rights reserved.

Licensed under the [MIT](LICENSE) license.

## Acknowledgements

[KYPO Team](https://crp.kypo.muni.cz/)
